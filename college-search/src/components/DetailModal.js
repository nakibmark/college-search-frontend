import { repo } from '../data/repo'
import { indexToTitle } from '../utils/index_to_readable'
import { Descriptions, Modal } from 'antd'
import React from 'react'
import { FieldDescription } from './FieldDescription'

export const DetailModal = ({ title, visible, onClose, record }) => {
  return (
    <Modal
      title={title}
      visible={visible}
      onOk={onClose}
      onCancel={onClose}
      width={1500}
    >
      <Descriptions bordered>
        {repo.fields.map((field, index) => (
          <Descriptions.Item key={index} label={indexToTitle[field]}>
            <FieldDescription field={field} record={record} />
          </Descriptions.Item>
        ))}
      </Descriptions>
    </Modal>
  )
}
