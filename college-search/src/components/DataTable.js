import React, { useCallback, useMemo, useState } from 'react'
import { Button, Table } from 'antd'
import { repo } from '../data/repo'
import { DetailModal } from './DetailModal'
import { calculateDistance } from '../utils/distance'

export const DataTable = () => {
  const [modalVisible, setModalVisible] = useState(false)
  const [openRecord, setOpenRecord] = useState({})
  const [locationStatus, setLocationStatus] = useState('')
  const [location, setLocation] = useState()

  const openDetails = (record) => {
    return {
      onClick: () => {
        setOpenRecord(record)
        setModalVisible(true)
      }
    }
  }

  const getLocation = () => {
    const success = (location) => {
      setLocation(location)
      setLocationStatus('Sorting by location')
    }
    const error = () => setLocationStatus('Error')
    if (!navigator.geolocation) {
      setLocationStatus('Not supported')
    } else {
      setLocationStatus('Locating…')
      navigator.geolocation.getCurrentPosition(success, error)
    }
  }

  const replaceCitySorter = useCallback(
    (columns) => {
      if (!location) return columns
      const myLat = location.coords.latitude
      const myLong = location.coords.longitude

      const distanceSorter = (a, b) => {
        const distanceA = calculateDistance(
          myLat,
          myLong,
          a.LATITUDE,
          a.LONGITUDE,
          'K'
        )
        const distanceB = calculateDistance(
          myLat,
          myLong,
          b.LATITUDE,
          b.LONGITUDE,
          'K'
        )
        return distanceA - distanceB
      }

      return columns.map((column) => {
        if (column.dataIndex === 'CITY')
          return {
            ...column,
            sorter: distanceSorter,
            sortOrder: 'ascend'
          }
        return column
      })
    },
    [location]
  )

  const getColumns = useMemo(() => {
    if (!location) return repo.columns
    return replaceCitySorter(repo.columns)
  }, [location, replaceCitySorter])

  return (
    <div className="dataTable">
      <Button className="locationButton" onClick={getLocation}>
        {locationStatus || 'Sort by location'}
      </Button>
      <Table
        dataSource={repo.schoolData}
        columns={getColumns}
        onRow={openDetails}
      />
      <DetailModal
        title={`Details for ${openRecord.INSTNM}`}
        visible={modalVisible}
        onClose={() => setModalVisible(false)}
        record={openRecord}
      />
    </div>
  )
}
