import React, { useState } from 'react'
import { repo, renderWebsiteAsLink } from '../data/repo'
import {
  indexToDegree,
  indexToLocale,
  indexToCarnegie
} from '../utils/index_to_readable'
import { Button, Popover } from 'antd'

const ShortenedProgramsDescription = ({ record, field }) => {
  const [popoverVisible, setPopoverVisible] = useState(false)
  return (
    <Popover
      content={record[field].map((program) => (
        <p>{repo.programData[program]}</p>
      ))}
      trigger="click"
      visible={popoverVisible}
      onVisibleChange={(vis) => setPopoverVisible(vis)}
    >
      <Button>Expand</Button>
    </Popover>
  )
}

export const FieldDescription = ({ record, field }) => {
  switch (field) {
    case 'PROGRAMS':
      return record[field] && record[field].length > 0 ? (
        <ShortenedProgramsDescription record={record} field={field} />
      ) : (
        'No data'
      )
    case 'HIGHDEG':
      return indexToDegree[record[field]]
    case 'LOCALE':
      return indexToLocale[record[field]]
    case 'CCSIZSET':
      return indexToCarnegie[record[field]]
    case 'INSTURL':
      return renderWebsiteAsLink(record[field])
    default:
      return record[field]
  }
}
