import React from 'react'
import schools from './ma_schools.json'
import programs from './programs.json'
import { indexToTitle } from '../utils/index_to_readable'

const renderNullAsDash = (text) => (text === 'NULL' ? '-' : text)

export const renderWebsiteAsLink = (text) => {
  if (!text.startsWith('https://') && !text.startsWith('http://')) {
    text = `https://${text}`
  }
  const url = new URL(text)
  return <a href={url.href}>{url.href}</a>
}

const renderZipCodes = (text) => text.slice(0, 5)

export const repo = {
  schoolData: schools,
  programData: programs,
  fields: Object.keys(schools[0]),
  columns: [
    {
      title: indexToTitle['INSTNM'],
      dataIndex: 'INSTNM',
      sorter: (a, b) => a.INSTNM.localeCompare(b.INSTNM)
    },
    {
      title: indexToTitle['CITY'],
      dataIndex: 'CITY',
      sorter: (a, b) => a.CITY.localeCompare(b.CITY)
    },
    {
      title: indexToTitle['STABBR'],
      dataIndex: 'STABBR'
    },
    {
      title: indexToTitle['ZIP'],
      dataIndex: 'ZIP',
      render: renderZipCodes
    },
    {
      title: indexToTitle['INSTURL'],
      dataIndex: 'INSTURL',
      render: renderWebsiteAsLink
    },
    {
      title: indexToTitle['ADM_RATE'],
      dataIndex: 'ADM_RATE',
      filters: [{ text: 'Not Empty', value: 'NULL' }],
      onFilter: (_, record) => record.ADM_RATE !== 'NULL',
      render: renderNullAsDash,
      sorter: (a, b) => a.ADM_RATE - b.ADM_RATE
    },
    {
      title: indexToTitle['SAT_AVG'],
      dataIndex: 'SAT_AVG',
      key: 'SAT_AVG',
      filters: ['Not Empty'],
      onFilter: (_, record) => record.SAT_AVG !== 'NULL',
      render: renderNullAsDash,
      sorter: (a, b) => a.SAT_AVG - b.SAT_AVG
    }
  ]
}
