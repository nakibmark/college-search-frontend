This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# TL:DR;
Just use `npm start` and test it out.

## Notes
I tried organizing this solution the way I would an actual application. I made a static data layer to pull from `repo.js` so that I didn't have to read directly from my data source every time, much like we'd do if we were storing data fetched from an API. I separated out all the components as much as I thought made sense for scope and inheritance. I didn't bother trying to parse the CSV (I mean, I did, but I found it to be an annoying waste of time since it was formatted visually like an Excel sheet, and not actually like a data source), instead I copied the relevant human-readable lookups to a util file.

I tried for a bonus location sort, but I couldn't really get it to work since my sorter callbacks are static and I couldn't think of a reasonable way to modify them in-flight within the time I spent on this. Given enough time I'd like to figure out a cool solution here. I think I'm 90% there.

I made heavy use of [Ant Design](ant.design) because building all that visual stuff from scratch would have taken me way longer than four hours.

Let me know if there's anything else I can answer for you, and thanks for your time/consideration!

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
